booleanValue = true;
numericValue = 10;
numericValueDouble = 15.0;
stringValue = "Vovchik";
stringValue2 = 'Vovchik';
stringValue3 = `Vovchik age is ${numericValue}`;
bigIntValue = 10000n;
symbolValue = Symbol('mySymbolDescription');
nullValue = null;
objectValue = { stringValue4: "Vovka", stringValue5: "Oleh" }


console.log(typeof undefinedDataType);
console.log(typeof booleanValue);
console.log(typeof numericValue);
console.log(typeof numericValueDouble);
console.log(typeof stringValue);
console.log(typeof stringValue2);
console.log(typeof stringValue3);
console.log(typeof bigIntValue);
console.log(typeof symbolValue);
console.log(typeof nullValue);
console.log(typeof objectValue);

console.log("11"==11);
console.log("11"===11);
