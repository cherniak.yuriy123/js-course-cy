function canSmoke(age) {
    if (age < 18) {
        console.log("You haven`t access to buy cigarettes");
        return
    }
    console.log("You`re welcome, You can buy cigarettes");
}

let canSmoke2 = function (age) {
    if (age < 18) {
        console.log("You haven`t access to buy cigarettes");
        return;
    }
    console.log("You`re welcome, You can buy cigarettes");
}

let canSmoke3 = (age) => {
    if (age < 18) {
        console.log("You haven`t access to buy cigarettes");
        return;
    }

    console.log("You`re welcome, You can buy cigarettes");
}

canSmoke(16);
canSmoke2(21);
canSmoke3(1);

function allowedToIn(name, age, department) {
    if (name == "Vadym" && age == 40 && department == "QA") {
        return console.log("Vadym is great QA engineer");
    }

    return console.log("He is not Vadym QA");
}

allowedToIn("Vadym", 41, "QA")
allowedToIn("Vadym2", 40, "QA")
allowedToIn("Vadym", 40, "QAs")
allowedToIn("Vadym", 40, "QA")
